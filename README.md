# ansible-collection-community

Ansible collection to create a community server based on Discourse.

## Licence

[![Copyright 2018-2020 Chaudier Christophe](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)](https://choosealicense.com/licenses/agpl-3.0/)

**ansible-collection-community** is created by Chaudier Christophe and published under the license [AGPL3](https://www.gnu.org/licenses/agpl.html).

---

Icon made by [Pixel perfect](https://icon54.com/) from [www.flaticon.com](https://www.flaticon.com)
